# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130508134555) do

  create_table "checkins", :force => true do |t|
    t.boolean  "checked"
    t.integer  "habit_id"
    t.integer  "client_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "clients", :force => true do |t|
    t.string   "email"
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "surname"
    t.string   "phone_number"
    t.string   "state"
    t.text     "note"
    t.date     "dob"
    t.integer  "coach_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "clients", ["coach_id"], :name => "index_clients_on_coach_id"
  add_index "clients", ["email"], :name => "index_clients_on_email", :unique => true
  add_index "clients", ["reset_password_token"], :name => "index_clients_on_reset_password_token", :unique => true

  create_table "clients_groups", :id => false, :force => true do |t|
    t.integer "client_id"
    t.integer "group_id"
  end

  add_index "clients_groups", ["client_id", "group_id"], :name => "index_clients_groups_on_client_id_and_group_id"
  add_index "clients_groups", ["group_id", "client_id"], :name => "index_clients_groups_on_group_id_and_client_id"

  create_table "coaches", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "surname"
    t.string   "brand_name"
    t.string   "phone_number"
    t.string   "state"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "coaches", ["email"], :name => "index_coaches_on_email", :unique => true
  add_index "coaches", ["reset_password_token"], :name => "index_coaches_on_reset_password_token", :unique => true

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "goals", :force => true do |t|
    t.string   "title"
    t.string   "state"
    t.text     "description"
    t.date     "deadline"
    t.integer  "client_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "goals", ["client_id"], :name => "index_goals_on_client_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "state"
    t.text     "note"
    t.integer  "coach_id"
    t.integer  "client_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "groups", ["client_id"], :name => "index_groups_on_client_id"
  add_index "groups", ["coach_id"], :name => "index_groups_on_coach_id"

  create_table "habits", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.date     "accomplished_at"
    t.integer  "client_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "frequency"
    t.string   "state"
  end

  create_table "messages", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "client_id"
    t.integer  "coach_id"
    t.integer  "group_id"
    t.integer  "delayed_job_id"
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.datetime "send_at"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "state"
  end

  add_index "messages", ["client_id"], :name => "index_messages_on_client_id"
  add_index "messages", ["coach_id"], :name => "index_messages_on_coach_id"

end
