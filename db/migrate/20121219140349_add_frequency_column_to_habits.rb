class AddFrequencyColumnToHabits < ActiveRecord::Migration
  def change
    add_column :habits, :frequency, :string
  end
end
