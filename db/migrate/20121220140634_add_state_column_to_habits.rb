class AddStateColumnToHabits < ActiveRecord::Migration
  def change
    add_column :habits, :state, :string
  end
end
