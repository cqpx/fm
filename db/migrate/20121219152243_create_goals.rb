class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.string :title
      t.string :state
      t.text :description
      t.date :deadline
      t.integer :client_id

      t.timestamps
    end
    add_index :goals, :client_id
  end
end
