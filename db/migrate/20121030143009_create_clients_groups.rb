class CreateClientsGroups < ActiveRecord::Migration
  def change
    create_table :clients_groups, :id => false do |t|
      t.integer :client_id
      t.integer :group_id
    end

    add_index :clients_groups, [:client_id, :group_id]
    add_index :clients_groups, [:group_id, :client_id]
  end
end
