class ChangeMessageTypeToInteger < ActiveRecord::Migration
  def up
  connection.execute(%q{
    alter table messages
    alter column message_type
    type integer using cast(message_type as integer)
  })
end

  def down
    change_column :messages, :message_type, :string
  end
end
