class CreateHabits < ActiveRecord::Migration
  def change
    create_table :habits do |t|
      t.string :title
      t.text :content
      t.date :accomplished_at
      t.integer :client_id

      t.timestamps
    end
  end
end
