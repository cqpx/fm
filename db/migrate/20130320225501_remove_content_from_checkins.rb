class RemoveContentFromCheckins < ActiveRecord::Migration
  def up
    remove_column :checkins, :content
  end

  def down
    add_column :checkins, :content, :string
  end
end
