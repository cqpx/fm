class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.string :content
      t.boolean :checked

      t.integer :habit_id
      t.integer :client_id

      t.timestamps
    end
  end
end
