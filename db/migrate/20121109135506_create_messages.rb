class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :title
      t.text :content

      t.integer :client_id
      t.integer :coach_id
      t.integer :group_id
      t.integer :delayed_job_id

      t.integer :sender_id
      t.string :sender_type
      t.integer :receiver_id
      t.string :receiver_type

      t.datetime :send_at

      t.timestamps
    end
    add_index :messages, :coach_id
    add_index :messages, :client_id
  end
end
