class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :state
      t.text :note

      t.integer :coach_id
      t.integer :client_id

      t.timestamps
    end
    add_index :groups, :coach_id
    add_index :groups, :client_id
  end
end
