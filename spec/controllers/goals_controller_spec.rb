require 'spec_helper'

describe GoalsController do
  let(:goal) { FactoryGirl.create :goal }
  before(:each) do
    request.env["HTTP_REFERER"] = root_path
  end

  context "coach signed in" do
    before(:each) do
      sign_in goal.client.coach
    end

    it "POST /create" do
      expect {
        post :create, {
          "goal" => {
            "title" => "get up early",
            "description" => "get up early",
            "state" => "inactive",
            "deadline" => "2013-01-17",
          },
          "client_id" => goal.client.id,
        }
      }.to change{
        goal.client.goals.count
      }.by(1)
    end

    it "POST /create failed" do
      expect {
        post :create, {
          "goal" => {
            "description" => "get up early",
            "state" => "inactive",
            "deadline" => "2013-01-17",
          },
          "client_id" => goal.client.id,
        }
      }.to_not change{
        goal.client.goals.count
      }.by(1)
    end

    it "GET /new" do
      get :new, client_id: goal.client.id
      response.should be_success
    end

    it "DELETE /:id" do
      delete :destroy, id: goal.id
      response.should be_redirect
    end

    it "PUT /update" do
      put :update, {
        "goal" => {
          "title" => "get up early",
          "description" => "get up early",
          "state" => "inactive",
          "deadline" => "2013-01-17",
        },
        "id" => goal.id,
      }

      goal.reload
      goal.title.should == "get up early"
    end

    it "GET /edit" do
      get :edit, id: goal.id
      response.should be_success
    end

    it "PUT /active" do
      put :active, id: goal.id
      goal.reload
      goal.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: goal.id
      goal.reload
      goal.state.should == "inactive"
    end
  end

  context "client signed in" do
    before(:each) do
      sign_in goal.client
    end

    it "GET /edit" do
      get :edit, id: goal.id
      response.should be_success
    end

    it "PUT /active" do
      put :active, id: goal.id
      goal.reload
      goal.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: goal.id
      goal.reload
      goal.state.should == "inactive"
    end
  end
end
