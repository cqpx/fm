require 'spec_helper'

describe PagesController do
  let(:coach) { FactoryGirl.create :coach }
  let(:client) { FactoryGirl.create :client }

  it "GET /index" do
    sign_in coach
    get :index
    response.should be_redirect
  end

  it "GET /index" do
    sign_in client
    get :index
    response.should be_redirect
  end

  it "GET /index" do
    get :index
    response.should be_success
  end
end
