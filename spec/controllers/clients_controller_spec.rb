require 'spec_helper'

describe ClientsController do
  let (:coach) { FactoryGirl.create :coach }

  it "guest GET /" do
    get :index, coach_id: coach.id
    response.should be_redirect
  end

  context "coach signed in" do
    before(:each) do
      sign_in coach
    end

    it "coach GET /" do
      3.times { coach.clients << FactoryGirl.create(:client, state: "active") }
      5.times { coach.clients << FactoryGirl.create(:client, state: "inactive") }

      get :index, coach_id: coach.id

      assigns(:active_clients).should be_present
      assigns(:inactive_clients).should be_present
      assigns(:active_clients).count.should == 3
      assigns(:inactive_clients).count.should == 5
    end

    it "coach GET /:id" do
      client = FactoryGirl.create(:client, state: "active", coach_id: coach.id)
      2.times { FactoryGirl.create(:habit, client_id: client.id, state: "active") }
      3.times { FactoryGirl.create(:habit, client_id: client.id, state: "inactive") }

      get :show, id: client.id

      assigns(:today_checkins).count.should == 2
    end

    it "GET /new" do
      get :new, coach_id: coach.id
      response.should be_success
    end

    it "POST /create" do
      expect {
        post :create, {
          "client" => {
            "email" => "client@example.com",
            "first_name" => "Mary",
            "surname" => "Lee",
            "phone_number" => "666666",
          },
          "coach_id" => coach.id,
        }
      }.to change{
        coach.clients.count
      }.by(1)
    end

    it "should not POST /create" do
      expect {
        post :create, {
          "client" => {
            "first_name" => "Mary",
            "surname" => "Lee",
            "phone_number" => "666666",
          },
          "coach_id" => coach.id,
        }
      }.to_not change{
        coach.clients.count
      }.by(1)
    end
  end

  context "coach sign in and have client" do
    before(:each) do
      request.env["HTTP_REFERER"] = root_path
      sign_in coach
      @client = FactoryGirl.create(:client, state: "active", coach: coach)
    end

    it "GET /goal" do
      get :goals, id: @client.id
      response.should be_success
    end

    it "DELETE /:id" do
      delete :destroy, id: @client.id
      response.should be_redirect
    end

    it "PUT /update" do
      put :update, {
        "client" => {
          "first_name" => "Mary",
          "surname" => "Lee",
          "email" => "mary@example.com",
          "phone_number" => "666666",
        },
        "id" => @client.id,
      }

      @client.reload
      @client.email.should == "mary@example.com"
    end

    it "PUT /active" do
      put :active, id: @client.id
      @client.reload
      @client.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: @client.id
      @client.reload
      @client.state.should == "inactive"
    end
  end
end
