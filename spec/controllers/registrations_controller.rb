require 'spec_helper'

describe Clients::RegistrationsController do
  let(:coach) { FactoryGirl.create :coach }
  let(:client) { FactoryGirl.create :client }

  it "ok" do
    client = Client.create(
      email: "900000@qq.com",
      coach_id: coach.id,
      first_name: "FName",
      surname: "SName",
      phone_number: "123456"
    )
    Client.count.should == 1

    @request.env["devise.mapping"] = Devise.mappings[:client]
    post :create, "client"=>{
      "first_name"=>"Who am I",
      "surname"=>"fwoiejfiowejf",
      "phone_number"=>"234234234",
      "email"=>"900000@qq.com",
      "coach_id"=>"2",
      "dob"=>"",
      "password"=>"[FILTERED]",
      "password_confirmation"=>"[FILTERED]"
    }
    Client.count.should == 1
    Client.last.first_name.should == "Who am I"

    response.should redirect_to(client_path(Client.last))
  end
end
