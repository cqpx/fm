require 'spec_helper'

describe MessagesController do
  let(:coach) { FactoryGirl.create :coach }
  let(:px) { FactoryGirl.create :coach, email: "cqpanxu@gmail.com" }
  let(:client) { FactoryGirl.create :client }
  let(:client_px) { FactoryGirl.create :client, email: "cqpanxu@gmail.com" }

  context "coach signed in" do
    before do
      sign_in coach
      client_px.save
      # @data = {
      #   "mandrill_events" => mandrill_events.to_s
      # }
    end

    # it "ok" do
    #   post :inbound, @data
    #   response.should be_success
    # end

    #it "don't create new messages with email is not involved" do
      #expect {
        #post :inbound, @data
      #}.to change {
        #Message.count
      #}.by(1)
    #end

    # it "create new messages with email is involved" do
    #   expect {
    #     post :inbound, @data
    #   }.to change {
    #     Message.count
    #   }.by(1)
    # end

    it "clean email body" do
      %Q{
        Hello
        :: Reply above this line ::
        Yelp
      }.should include("Yelp")

      MessagesController.clean_body(%Q{
        Hello
        :: Reply above this line ::
        Yelp
      }).should_not include("Yelp")
    end
  end

  it "send message to coach" do
    sign_in client
    request.env["HTTP_REFERER"] = client_path(client)
    coach = client.coach
    post :create, {
      "message" => {
        "title" => "Hi coach",
        "content" => "how are you",
      },
      "coach_id" => client.to_param
    }
    coach.messages.count.should == 1
  end

  describe "GET" do
    let(:coach) { FactoryGirl.create :coach }
    let(:client) { FactoryGirl.create :client }

    before(:each) do
      sign_in coach
    end

    it "send message to client" do
      request.env["HTTP_REFERER"] = coach_path(coach)
      client = FactoryGirl.create(:client, state: "active", coach_id: coach.id)
      post :create, {
        "client_ids" => [client.id],
        "message" => {
          "title" => "Hi client",
          "content" => "how are you",
          "send_at_date" => "2013-01-30",
          "send_at_time" => "06:12 PM",
        }
      }
      client.messages.count.should == 1
    end

    it "GET /" do
      get :index, {"coach_id" => coach.id}
      response.should be_success
    end

    it "GET /new" do
      get :new
      response.should be_success
    end

    it "DELETE /:id" do
      message = FactoryGirl.build :message
      message.sender = coach
      message.save
      delete :destroy, id: message.id
      response.should be_redirect
    end

    it "POST /confirm" do
      request.env["HTTP_REFERER"] = root_path
      message = FactoryGirl.build :message
      message.sender = coach
      message.save
      post :confirm, id: message.id
      response.should redirect_to(root_path)
    end
  end
end
