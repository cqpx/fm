require 'spec_helper'

describe HabitsController do
  let(:habit) { FactoryGirl.create :habit }

  before(:each) do
    request.env["HTTP_REFERER"] = root_path
  end

  context "coach signed in" do
    before(:each) do
      sign_in habit.client.coach
    end

    it "POST /create" do
      expect {
        post :create, {
          "habit" => {
            "title" => "get up early",
            "content" => "get up early",
            "state" => "active",
            "accomplished_at" => "2013-01-17",
            "frequency" => "everyday"
          },
          "client_id" => habit.client.id,
        }
      }.to change{
        habit.client.habits.count
      }.by(1)
    end

    it "POST /create with 3 active habits exist" do
      client = habit.client
      3.times { FactoryGirl.create :habit, client: client, state: "active" }
      client.habits.where(state: "active").count.should >= 3

      post :create, {
        "habit" => {
          "title" => "get up early",
          "content" => "get up early",
          "state" => "active",
          "accomplished_at" => "2013-01-17",
          "frequency" => "everyday"
        },
        "client_id" => client.id,
      }

      client.habits.last.should be_inactive
    end

    it "POST /create failed" do
      expect {
        post :create, {
          "habit" => {
            "title" => "get up early",
            "state" => "active",
            "accomplished_at" => "2013-01-17",
          },
          "client_id" => habit.client.id,
        }
      }.to_not change{
        habit.client.goals.count
      }.by(1)
    end

    it "DELETE /:id" do
      delete :destroy, id: habit.id
      response.should be_redirect
    end

    it "PUT /update" do
      put :update, {
        "habit" => {
          "title" => "get up early",
          "content" => "get up early",
          "state" => "active",
          "accomplished_at" => "2013-01-17",
          "frequency" => "everyday"
        },
        "id" => habit.id,
      }

      habit.reload
      habit.title.should == "get up early"
    end

    it "PUT /active" do
      put :active, id: habit.id
      habit.reload
      habit.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: habit.id
      habit.reload
      habit.state.should == "inactive"
    end
  end

  context "client signed in" do
    before(:each) do
      sign_in habit.client
    end

    it "PUT /active" do
      put :active, id: habit.id
      habit.reload
      habit.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: habit.id
      habit.reload
      habit.state.should == "inactive"
    end
  end
end
