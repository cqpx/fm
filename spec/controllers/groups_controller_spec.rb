require 'spec_helper'

describe GroupsController do
  let(:group) { FactoryGirl.create :group }
  let(:inactive_group) { FactoryGirl.create :group, state: "inactive" }

  before(:each) do
    request.env["HTTP_REFERER"] = root_path
  end

  context "coach signed in" do
    before(:each) do
      sign_in group.coach
    end

    it "GET /new" do
      get :new, coach_id: group.coach.id
      response.should be_success
    end

    it "POST /create" do
      c1 = FactoryGirl.create :client
      c2 = FactoryGirl.create :client

      expect {
        post :create, {
          "group" => {
            "name" => "diy",
            "note" => "diy",
            "state" => "active",
            "client_ids" => ["", c1.id.to_s, c2.id.to_s]
          },
          "coach_id" => group.coach.id,
        }
      }.to change{
        group.coach.groups.count
      }.by(1)
    end

    it "GET /info" do
      get :info, id: group.id
      response.should be_success
    end

    it "GET /index" do
      get :index, coach_id: group.coach.id
      response.should be_success
    end

    it "GET /show" do
      get :show, id: group.id
      response.should be_success
    end

    it "DELETE /:id" do
      delete :destroy, id: group.id
      response.should be_redirect
    end

    it "PUT /update" do
      c1 = FactoryGirl.create :client
      c2 = FactoryGirl.create :client

      put :update, {
        "group"=>{
          "name" => "joker",
          "client_ids"=> ["", c1.id.to_s, c2.id.to_s],
          "note"=> "joke"
        },
        id: group.id
      }

      group.reload
      group.clients.count.should == 2
      group.name.should == "joker"
    end

    it "GET /edit" do
      get :edit, id: group.id
      response.should be_success
    end

    it "PUT /active" do
      sign_in inactive_group.coach
      put :active, id: inactive_group.id
      inactive_group.reload
      inactive_group.state.should == "active"
    end

    it "PUT /deactive" do
      put :deactive, id: group.id
      group.reload
      group.state.should == "inactive"
    end
  end
end
