require 'spec_helper'

describe CoachStepsController do
  let(:coach) { FactoryGirl.create :coach }

  before do
    sign_in coach
  end

  it "show clients step" do
    get :show, id: :clients
    response.should be_success
  end

  it "create clients" do
    put :update, {
      "coach"=>{
        "clients_attributes"=>{
          "70194595639800"=>{
            "first_name"=>"Xu",
            "surname"=>"Pan",
            "phone_number"=>"1213123",
            "email"=>"xu.pan@intridea.com"
          },
          "70194581181480"=>{
            "first_name"=>"",
            "surname"=>"",
            "phone_number"=>"",
            "email"=>""
          },
          "70194564719360"=>{"first_name"=>"", "surname"=>"", "phone_number"=>"", "email"=>""},
          "70194592953720"=>{"first_name"=>"", "surname"=>"", "phone_number"=>"", "email"=>""},
          "70194593293860"=>{"first_name"=>"", "surname"=>"", "phone_number"=>"", "email"=>""}
        }
      },
      "commit"=>"Next",
      "id"=>"clients"
    }

    Client.count.should == 1
  end

  it "put into groups" do
    client = FactoryGirl.create(:client, state: "active", coach_id: coach.id)
    put :update, {
      "coach"=>{
        "groups_attributes"=>{
          "70293041940640"=>{
            "name"=>"group_one",
            "client_ids"=>["", client.id]
          },
          "70292998405380"=>{
            "name"=>"",
            "client_ids"=>[""]
          },
          "70293042049440"=>{
            "name"=>"", "client_ids"=>[""]
          },
          "70293042196980"=>{
            "name"=>"", "client_ids"=>[""]
          },
          "70292998582900"=>{
            "name"=>"", "client_ids"=>[""]
          }
        }
      },
      "id"=>"groups"
    }

    Group.count.should == 1
  end

  it "send messages" do
    client = FactoryGirl.create(:client, state: "active", coach_id: coach.id)
    group = FactoryGirl.create(:group, coach_id: coach.id, client_id: client.id)
    put :update, {
      "message"=>{
        "title"=>"hello",
        "content"=>"ddd",
        "send_at_date"=>"2013-01-22",
        "send_at_time"=>"12:13 PM"
      },
      "client_ids"=>[client.id],
      "group_ids"=>[group.id],
      "id"=>"messages"
    }
    client.messages.count.should == 1
    group.messages.count.should == 1
  end
end
