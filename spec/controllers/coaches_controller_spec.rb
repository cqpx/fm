require 'spec_helper'

describe CoachesController do
  let(:coach) { FactoryGirl.create :coach }

  before(:each) do
    sign_in coach
  end

  it "GET /edit" do
    get :edit_profile, coach_id: coach.id
    response.should be_success
  end

  it "PUT /update" do
    put :update_profile, {
      "coach" => {
        "first_name" => "Ann",
        "surname" => "White",
        "brand_name" => "company",
        "email" => "ann@gmail.com",
      },
      "id" => coach.id,
    }

    coach.reload
    coach.brand_name.should == "company"
  end
end
