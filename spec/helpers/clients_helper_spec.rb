require 'spec_helper'

describe ClientsHelper do
  it ".fancy_birthday" do
    Timecop.freeze(Date.new(2012,11,1))
    bd = Date.new(1986,11,26)
    result = helper.fancy_birthday(bd)
    result.should == "in 25 days time"
  end
end
