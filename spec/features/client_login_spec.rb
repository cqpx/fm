require 'spec_helper'

def login client
  visit new_client_session_path

  fill_in "Email", with: client.email
  fill_in "Password", with: "qwe123"
  click_on "Sign in"
end

describe "Client" do
  let (:client) { FactoryGirl.create :client }
  let (:coach) { FactoryGirl.create :coach }

  it "could login" do
    login client

    page.should have_content("Signed in successfully.")
    current_path.should == dashboard_client_path(client)
  end
end
