require 'spec_helper'

describe Client do
  let(:client) { FactoryGirl.create :client }
  let(:active_group) { FactoryGirl.create :group, state: "active" }
  let(:inactive_group) { FactoryGirl.create :group, state: "inactive" }

  context "birthday parsing" do
    it "birthday should be parsed" do
      client = FactoryGirl.create :client, dob: "21/11/09"
      client.dob.should == Date.new(2009, 11, 21)
    end

    it "empty dob won't raise error" do
      client = FactoryGirl.create :client, dob: ""
      client.dob.should == nil
    end
  end

  context "#enrolled_in_any_active_group?" do
    before do
      client.groups << active_group
      client.groups << inactive_group
    end

    it "should be true" do
      client.should be_enrolled_in_any_active_group
    end

    it "should be false if unenroll from the active group" do
      client.groups.delete active_group
      client.should_not be_enrolled_in_any_active_group
    end

    it "should be false if unenroll from the inactive group" do
      client.groups.delete inactive_group
      client.should be_enrolled_in_any_active_group
    end
  end

  it "new group is inactive" do
    client.should be_inactive
  end

  it "keep inactive if it only enrolled in a inactive group" do
    client.groups << inactive_group
    client.should be_inactive
  end

  it "is active if it enrolled in a inactive group and then an active group" do
    client.groups << inactive_group
    client.groups << active_group
    client.should be_active
  end

  it "is active if enrolled in an active group" do
    client.groups << active_group
    client.should be_active
  end

  it "won't be deactivated if enrolled in a inactive group after an active group" do
    client.groups << active_group
    client.groups << inactive_group
    client.should be_active
  end

  it "is inactive if all enrolled groups are deactivated" do
    client.groups << active_group
    client.groups << inactive_group

    active_group.deactivate

    client.reload
    client.should be_inactive
  end

  context ".coach_unreplyed_new_messages_count" do
    it "has messages" do
      Timecop.travel(Date.new(2012, 12, 12) - 10.months)
      FactoryGirl.create(:message, sender: client, receiver: client.coach)
      Timecop.travel(Date.new(2012, 12, 12) - 9.months)
      FactoryGirl.create(:message, sender: client.coach, receiver: client)
      Timecop.travel(Date.new(2012, 12, 12) - 8.months)
      3.times { |i| FactoryGirl.create :message, sender: client, receiver: client.coach }
      client.coach_unreplied_messages_count.should == 3
    end

    it "coach send last" do
      time = Time.zone.now - 10.months
      FactoryGirl.create :message, sender: client, receiver: client.coach, send_at: time
      FactoryGirl.create :message, sender: client.coach, receiver: client, send_at: time + 1.hour
      3.times { |i| FactoryGirl.create :message, sender: client, receiver: client.coach, send_at: time + (2+i).hours }
      FactoryGirl.create :message, sender: client.coach, receiver: client, send_at: time + 10.hour
      client.coach_unreplied_messages_count.should == 0
    end
  end

  it "#messaged_last?" do
    Message.send_to_coach({
      title: "afaf",
      content: "wefwef",
      send_at_date: "2012-11-12",
      send_at_time: "11:00"
    }, client)
    client.reload
    client.sent_messages.count.should == 1
    client.messages.count.should == 0
    client.messaged_last?.should == true

    Message.send_to_clients({
      title: "afaf",
      content: "wefwef",
      send_at_date: "2012-11-12",
      send_at_time: "11:10"
    }, [client.id], client.coach)
    client.reload
    client.sent_messages.count.should == 1
    client.messages.count.should == 1
    client.messaged_last?.should == false

    Message.send_to_coach({
      title: "afaf",
      content: "wefwef",
      send_at_date: "2012-11-12",
      send_at_time: "11:20"
    }, client)
    client.reload
    client.sent_messages.count.should == 2
    client.messages.count.should == 1
    client.messaged_last?.should == true
  end
end
