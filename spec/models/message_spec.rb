require 'spec_helper'

describe Message do
  let(:coach) { FactoryGirl.create :coach }
  let(:client) { FactoryGirl.create :client }

  it "parse date" do
    message = FactoryGirl.create :message,
      send_at_date: "2012-12-13",
      send_at_time: "14:25"

    message.send_at.should == DateTime.new(2012, 12, 13, 14, 25)
  end

  it "confirming type 0 message should schedule a email" do
    message = FactoryGirl.create :message,
      send_at_date: "2012-12-13",
      send_at_time: "14:25",

    message.confirm
    Delayed::Job.count.should == 1
    Delayed::Job.last.id.should == message.delayed_job_id
  end

  it "destroy a message with scheduled job will also destroy it's job" do
    message = FactoryGirl.create :message,
      send_at_date: "2012-12-13",
      send_at_time: "14:25",

    message.confirm
    Delayed::Job.count.should == 1
    message.destroy
    Delayed::Job.count.should == 0
  end

  it "confirming type 1 message shouldn't schedule a email" do
    message = FactoryGirl.create :message
    message.confirm
    Delayed::Job.count.should == 0
  end

  it "#send_to_clients" do
    Timecop.travel Time.new(2012,12,11,11,11)
    3.times { FactoryGirl.create(:client, coach_id: coach.id) }
    Message.send_to_clients(
      {
        title: "hi",
        content: "nihao",
        send_at_date: "2012-12-11",
        send_at_time: "11:11",
        message_type: 1
      },
      coach.clients.map(&:id),
      coach
    )
    coach.clients.each do |client|
      client.messages.count.should == 1
      client.messages.last.sender.should == coach
    end
  end

  it "#send_to_groups" do
    Timecop.travel Time.new(2012,12,11,11,11)
    3.times { FactoryGirl.create(:group, coach_id: coach.id) }
    Message.send_to_groups(
      {
        title: "hi",
        content: "nihao",
        send_at_date: "2012-12-11",
        send_at_time: "11:11",
      },
      coach.groups.map(&:id),
      coach
    )
    coach.groups.each do |group|
      group.messages.count.should == 1
      group.messages.last.sender.should == coach
    end
  end

  it "#send_to_coach" do
   Timecop.travel Time.new(2012,12,11,11,11)
    3.times { FactoryGirl.create(:group, coach_id: coach.id) }
    Message.send_to_coach(
      {
        title: "hi",
        content: "nihao",
        send_at_date: "2012-12-11",
        send_at_time: "11:11",
      },
      client
    )
    client.coach.messages.count.should == 1
    client.coach.messages.last.sender.should == client
  end
end
