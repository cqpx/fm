# require 'spec_helper'

# describe Student do
#   let(:coach) { FactoryGirl.create :coach }
#   let(:client) { FactoryGirl.create :client }
#   let(:student) { FactoryGirl.create :student }

#   it "build relation with existing client" do
#     coach.clients << client
#     s = FactoryGirl.create :student, email: client.email
#     s.clients.count.should == 1
#   end

#   it "parse birthday info" do
#     student = FactoryGirl.create :student, dob: "11/12"
#     student.dob.day.should == 11
#     student.dob.month.should == 12
#   end

#   it "parse birthday info with year" do
#     student = FactoryGirl.build :student, dob: "11/12/12"
#     student.dob.year.should == 2012
#     student.dob.day.should == 11
#     student.dob.month.should == 12
#   end

#   it "parse birthday info" do
#     expect {
#       FactoryGirl.build :student, dob: "99/12"
#     }.to_not raise_error(ArgumentError)
#   end

#   it "create client info" do
#     expect {
#       student = FactoryGirl.create :student
#     }.to change{
#       Client.count
#     }.by(1)
#   end

#   it "create client info" do
#     student = FactoryGirl.create :student
#     client = Client.last
#     client.email.should == student.email
#     client.dob.should == student.dob
#     client.first_name.should == student.first_name
#     client.surname.should == student.surname
#     client.coach_id.should == student.coach_id
#   end

#   it "update client info" do
#     email = 'cqpanxu@gmail.com'
#     client = FactoryGirl.create :client, email: email
#     student = FactoryGirl.create :student, email: email
#     client.reload
#     student.dob.should == client.dob
#     student.first_name.should == client.first_name
#     student.surname.should == client.surname
#   end
# end
