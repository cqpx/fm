require 'spec_helper'

describe Group do
  let(:coach) { FactoryGirl.create :coach }
  let(:group) { FactoryGirl.create :group }
  let(:active_group) { FactoryGirl.create :group, state: "active" }
  let(:other_active_group) { FactoryGirl.create :group, state: "active" }
  let(:inactive_group) { FactoryGirl.create :group, state: "inactive" }

  let(:active_client) { FactoryGirl.create :client, state: "active" }
  let(:inactive_client) { FactoryGirl.create :client, state: "inactive" }
  let(:active_client_with_other_active_group) do
    client = FactoryGirl.create :client, state: "active"
    client.groups << other_active_group
    client
  end

  def reload_all
    active_client.reload
    active_client_with_other_active_group.reload
    inactive_client.reload
  end

  it "new group is inactive" do
    group.should be_active
  end

  context "active group" do
    before do
      active_group.clients << active_client
      active_group.clients << active_client_with_other_active_group
      active_group.clients << inactive_client

      reload_all
    end

    specify { active_group.clients.each { |c| c.should be_active } }

    context "deactivate" do
      before do
        active_group.deactivate

        reload_all
      end

      specify { active_client.should be_inactive }
      specify { active_client_with_other_active_group.should be_active }
      specify { inactive_client.should be_inactive }
    end

    context "delete" do
      before do
        active_group.clients.each { |c| active_group.clients.delete c }

        reload_all
      end

      specify { active_client.should be_inactive }
      specify { active_client_with_other_active_group.should be_active }
      specify { inactive_client.should be_inactive }
    end
  end

  context "inactive group" do
    before do
      inactive_group.clients << active_client
      inactive_group.clients << active_client_with_other_active_group
      inactive_group.clients << inactive_client

      reload_all
    end

    specify { active_client.should be_active }
    specify { active_client_with_other_active_group.should be_active }
    specify { inactive_client.should be_inactive }

    context "activated" do
      before do
        inactive_group.activate

        reload_all
      end

      specify { inactive_group.clients.each { |c| c.should be_active } }
    end

    context "delete" do
      before do
        inactive_group.clients.each { |c| inactive_group.clients.delete c }

        reload_all
      end

      specify { active_client.should be_inactive }
      specify { active_client_with_other_active_group.should be_active }
      specify { inactive_client.should be_inactive }
    end
  end

  it "#broadcast_to_clients" do
    group.clients << active_client
    group.broadcast_to_clients FactoryGirl.create(:message)
    active_client.messages.count.should == 1
  end

  it "send message to group will broadcast to clients enrolled in the group" do
    client = FactoryGirl.create :client
    coach = group.coach
    coach.clients << client
    group.clients << client
    group.clients.last.should == client
    coach.clients.last.should == client
    group.coach.should == client.coach
    Message.send_to_groups({
      title: "woifef",
      content: "fowiefje",
      message_type: 1,
      send_at_date: "2012-12-12",
      send_at_time: "11:11"
    }, [group.id], coach)
    client.reload
    client.messages.last.content.should == group.messages.last.content
  end
end
