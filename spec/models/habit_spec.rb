require 'spec_helper'

describe Habit do
  it "create an active habit should create checkin " do
    habit = FactoryGirl.create :habit, state: "active"
    habit.checkins.count.should == 1
    habit.checkins.last.content.should == habit.content
  end

  it "create an inactive habit won't create checkin" do
    habit = FactoryGirl.create :habit, state: "inactive"
    habit.checkins.count.should == 0
  end
end
