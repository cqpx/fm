FactoryGirl.define do
  factory :coach do
    first_name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    brand_name { Faker::Name.first_name }
    email { Faker::Internet.email }
    password "123456"
    password_confirmation "123456"
    phone_number 123456
  end
end
