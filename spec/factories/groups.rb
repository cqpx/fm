# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :group do
    name { Faker::Name.name }
    note { Faker::Lorem.paragraph }
    coach
  end
end
