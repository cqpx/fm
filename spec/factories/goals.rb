# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :goal do
    title { Faker::Name.first_name }
    state "inactive"
    description { Faker::Name.first_name }
    deadline { Date.today + 10.days }
    client
  end
end
