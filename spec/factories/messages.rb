# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    title { Faker::Lorem.word }
    content { Faker::Lorem.sentence }
    association :sender, factory: :coach
    association :receiver, factory: :client
    send_at { Time.zone.now }
  end
end
