# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :client do
    first_name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    phone_number 123456
    email { Faker::Internet.email }
    note { Faker::Lorem.paragraph }
    password 'qwe123'
    password_confirmation 'qwe123'
    coach
  end
end
