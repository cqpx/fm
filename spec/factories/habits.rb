# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :habit do
    title { Faker::Lorem.word }
    content { Faker::Lorem.word }
    accomplished_at { Date.today + 3.days }
    client
    frequency { Faker::Lorem.sentence }
    state { ["active", "inactive"].sample }
  end
end
