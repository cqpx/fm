# set :bundle_cmd, 'source $HOME/.bash_profile && bundle'
require "rvm/capistrano"
require "bundler/capistrano"
require "delayed/recipes"

server "uxnap.com", :web, :app, :db, primary: true

set :rails_env, "production" #added for delayed job
after "deploy:stop",    "delayed_job:stop"
after "deploy:start",   "delayed_job:start"
after "deploy:restart", "delayed_job:restart"

# If you want to use command line options, for example to start multiple workers,
# define a Capistrano variable delayed_job_args:
#
#   set :delayed_job_args, "-n 2"

set :user, "deployer"
set :application, "fitness-manager"

set :deploy_to, "/home/#{user}/web/#{application}.uxnap.com"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, :git
set :repository, "git@bitbucket.org:cqpx/#{application}.git"
set :branch, "master"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup"
after "deploy", "deploy:migrate"

namespace :passenger do
  desc "Restart Application"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
end

after :deploy, "passenger:restart"
