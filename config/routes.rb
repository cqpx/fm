FitnessManager::Application.routes.draw do

  devise_for :coaches, controllers: {
    registrations: 'coaches/registrations'
  }
  devise_for :clients, controllers: {
    registrations: 'clients/registrations'
  }

  resources :coach_steps

  resources :messages do
    collection do
      post :inbound
      get :inbound
    end
    member do
      post :confirm
      get :reply
    end
  end

  resources :coaches, shallow: true do

    collection do
      get :settings
      get :edit_profile
      put :update_profile
    end

    resources :messages do
      collection do
        post :inbound
        get :inbound
        get :scheduled
      end
      member do
        post :confirm
      end
    end

    resources :clients do
      collection do
        get :checkins
      end
      resources :messages
      member do
        get :info
        get :goals
        get :dashboard
        put :active
        put :deactive
      end
      resources :habits do
        member do
          put :active
          put :deactive
        end
      end
      resources :goals do
        member do
          put :active
          put :deactive
        end
      end
    end

    resources :groups do
      resources :messages
      member do
        get :info
        put :active
        put :deactive
      end
    end
  end

  root :to => "pages#index"
  match '/about' => 'pages#about'
  match '/tips' => 'pages#tips'
  
end
