class Habit < ActiveRecord::Base
  attr_accessible :client_id, :content, :checked,
  :checked_at, :title, :accomplished_at, :frequency, :state
  validates :content, presence: true
  belongs_to :client
  has_many :checkins, dependent: :destroy
  after_create :create_checkin_for_today

  def create_checkin_for_today
    self.checkins.create(
      client_id: self.client_id,
    ) if self.active?
  end

  state_machine :state, :initial => :active do
    event :activate do
      transition :inactive => :active
    end

    event :deactivate do
      transition :active => :inactive
    end
  end

  scope :active, where(state: "active").order("id DESC")
  scope :inactive, where(state: "inactive").order("id DESC")
end
