class Coach < ActiveRecord::Base
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email,
    :password,
    :password_confirmation,
    :remember_me,
    :first_name,
    :surname,
    :brand_name,
    :clients_attributes,
    :groups_attributes,
    :brand_name,
    :phone_number

  state_machine :state, :initial => :inactive do
    event :activate do
      transition all => :active
    end

    event :deactivate do
      transition all => :inactive
    end
  end

  has_many :clients
  accepts_nested_attributes_for :clients, :reject_if => :all_blank
  has_many :checkins, through: :clients

  has_many :groups
  accepts_nested_attributes_for :groups, :reject_if => :all_blank

  has_many :sent_messages, as: :sender, class_name: "Message", dependent: :destroy
  has_many :messages, as: :receiver, dependent: :destroy

  validates :phone_number, :length => { :maximum => 10}, presence: true
  validates :first_name, :surname, :brand_name, :email, presence: true
  validates_format_of :phone_number,
    :message => "telephone number must contains only digits and plus sign.",
    :with => /^[+0-9]+$/,
    :if => :active?

  def name
    "#{first_name} #{surname}"
  end

  def to_param
    "#{id}-#{first_name.downcase}-#{surname.downcase}"
  end

  def past_messages
    Message.where("send_at <= ?", Time.zone.now).
      where("(sender_id = #{id} AND sender_type = 'Coach') OR (sender_type = 'Client' AND receiver_id = #{id})").
      where(state: "confirmed").
      order("send_at DESC")

      #page(params[:page])
  end

  def scheduled_messages
    self.sent_messages.
      where("NOT (send_at <= ? AND state = 'confirmed')", Time.zone.now).
      order("send_at DESC")

      #page(params[:page])
  end
end
