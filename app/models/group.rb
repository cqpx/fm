class Group < ActiveRecord::Base
  attr_accessible :name, :note, :client_ids, :state
  validates :name, presence: true

  belongs_to :coach
  has_many :messages, as: :receiver, dependent: :destroy

  has_and_belongs_to_many :clients,
    :after_add => :after_add,
    :after_remove => :after_remove

  def after_add client
    client.activate if active?
  end

  def after_remove client
    client.gently_deactivate
  end

  def broadcast_to_clients message
    message_hash = JSON.parse message.to_json(only: [:title, :content, :send_at_date, :send_at_time])
    self.clients.each do |client|
      m = client.messages.new message_hash
      m.sender = self
      m.save
    end
  end

  scope :active, where(state: "active")
  scope :inactive, where(state: "inactive")

  state_machine :state, :initial => :active do
    after_transition :on => :activate do |group, transition|
      group.clients.inactive.each {|c| c.activate}
    end

    after_transition :on => :deactivate do |group, transition|
      group.clients.active.each { |client| client.gently_deactivate }
    end

    event :activate do
      transition :inactive => :active
    end

    event :deactivate do
      transition :active => :inactive
    end
  end
end
