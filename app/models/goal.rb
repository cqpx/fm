class Goal < ActiveRecord::Base
  attr_accessible :client_id, :deadline, :description, :state, :title
  belongs_to :client
  validates :title, presence: true

  scope :active, where(state: "active").order(:deadline)
  scope :inactive, where(state: "inactive").order(:deadline)

  state_machine :state, :initial => :inactive do
    before_transition :on => :activate, :do => :deactivate_all_others
    event :activate do
      transition :inactive => :active
    end

    event :deactivate do
      transition :active => :inactive
    end
  end

  def deactivate_all_others
    self.client.goals.active.update_all(state: "inactive")
  end
end
