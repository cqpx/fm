class Checkin < ActiveRecord::Base
  attr_accessible :habit_id, :checked, :checked_at, :client_id
  scope :today, -> { where("date(checkins.created_at) = ?", Date.today) }

  belongs_to :habit
  belongs_to :client
end
