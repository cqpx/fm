class Message < ActiveRecord::Base
  attr_accessible :client_id,
    :coach_id,
    :content,
    :title,
    :delayed_job_id,
    :send_at,
    :send_at_date,
    :send_at_time,
    :sender
    :state

  before_save :confirm_if_past
  before_validation :set_datetimes # convert accessors back to db format
  validates :content, presence: true

  belongs_to :sender, polymorphic: true
  belongs_to :receiver, polymorphic: true

  before_destroy :destroy_delayed_jobs

  attr_accessor :send_at_date, :send_at_time

  after_initialize :get_datetimes # convert db format to accessors
  after_create :confirm_and_send_mail

  def confirm_if_past
    if self.send_at <= Time.now
      self.state = :confirmed
    end
  end

  def confirm_and_send_mail
    if self.send_at <= Time.now
      self.state = "confirmed"
      if self.receiver.is_a?(Client) || self.receiver.is_a?(Coach)
        self.send_mail
      elsif self.receiver.is_a?(Group)
        self.receiver.broadcast_to_clients(self)
      end
    end
  end

  def get_datetimes
    t = Time.now
    self.send_at ||= t-t.sec-t.min%15*60  # if the send_at time is not set, set it to now (rounded down to 0, 15, 30, 45 mins to match the timepicker)

    self.send_at_date ||= self.send_at.strftime("%Y-%m-%d")
    self.send_at_time ||= self.send_at.strftime("%I:%M %p")
  end

  def set_datetimes
    self.send_at = "#{self.send_at_date} #{self.send_at_time}:00" # convert the two fields back to db
  end

  def send_mail
    job = ClientMailer.delay(run_at: self.send_at).new_message(self)
    self.update_attributes(delayed_job_id: job.id)
  end

  # def self.send_to_coach message_hash, sender
  #   message = sender.coach.messages.new message_hash
  #   message.message_type = 1
  #   message.sender = sender
  #   message.save
  # end


  state_machine :state, :initial => :unconfirmed do
    after_transition :on => :confirm, :do => :send_mail

    event :confirm do
      transition :unconfirmed => :confirmed
    end

    event :unconfirm do
      transition :confirmed => :unconfirmed
    end

    event :send do
      transition :confirmed => :sent
    end
  end

  def self.send_to_clients message_hash, client_ids, sender
    client_ids.each do |cid|
      client = sender.clients.find cid
      m = client.messages.new message_hash
      m.sender = sender
      m.save
    end if client_ids.present?
  end

  def self.send_to_groups message_hash, group_ids, sender
    group_ids.each do |gid|
      group = sender.groups.find gid
      m = group.messages.new message_hash
      m.sender = sender
      m.save
    end if group_ids.present?
  end



  private

  def destroy_delayed_jobs
    Delayed::Job.destroy_all(id: self.delayed_job_id) if self.delayed_job_id
  end
end
