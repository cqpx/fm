class Client < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  attr_accessible :group_id,
    :email,
    :password,
    :password_confirmation,
    :remember_me,
    :first_name,
    :note,
    :phone_number,
    :surname,
    :group_ids,
    :state,
    :client_id,
    :coach_id,
    :checkins_attributes,
    :habits_attributes,
    :dob

  validates_format_of :phone_number,
    :message => "telephone number must containes only digits and plus sign.",
    :with => /^[+0-9]+$/,
    :if => :active?

  belongs_to :coach

  validates :email, :phone_number, :coach, :first_name, :surname,
    presence: true

  validates_uniqueness_of :email

  validates :phone_number, :length => { :maximum => 15}

  has_and_belongs_to_many :groups,
    :after_add => Proc.new { |client, group| client.activate if group.active? },
    :after_remove => Proc.new { |client, group| client.gently_deactivate }

  has_many :sent_messages, as: :sender, class_name: "Message", dependent: :destroy
  has_many :messages, as: :receiver, class_name: "Message", dependent: :destroy

  has_many :habits, dependent: :destroy
  accepts_nested_attributes_for :habits, allow_destroy: true
  has_many :goals, dependent: :destroy

  has_many :checkins, through: :habits
  accepts_nested_attributes_for :checkins

  def coach_replied_last?
    msg = self.messages.last
    msg.present? && msg.coach_id.present?
  end

  def gently_deactivate
    deactivate unless enrolled_in_any_active_group?
  end

  scope :active, where(state: "active")
  scope :inactive, where(state: "inactive")

  def today_checkins
    # check each a
    self.habits.active.each do |habit|
      habit.checkins.today.first_or_create(
        checked: false,
        client_id: self.id
      )
    end
    self.checkins.joins(:habit).where("habits.state ='active'").today
  end

  def latest_checkins
    self.checkins.where(checked: true).order("updated_at DESC").limit(3)
  end

  def enrolled_in_any_active_group?
    groups.where(state: "active").exists?
  end

  state_machine :state, :initial => :inactive do
    event :activate do
      transition all => :active
    end

    event :deactivate do
      transition all => :inactive
    end
  end

  def dob_with_format
    if self.dob.year == 1970
      "#{self.dob.day}/#{self.dob.month}"
    else
      "#{self.dob.day}/#{self.dob.month}/#{self.dob.year}"
    end
  end

  def name
    "#{first_name} #{surname}"
  end

  def dob=(dob)
    if dob.instance_of?(String) && dob.match(/\d+\/\d+\/\d+/)
      day, month, year = dob.split('/').map { |date| date.to_i }
      if year + 2000 > Date.today.year
        year += 1900
      else
        year += 2000
      end

      dob = Date.new(year, month, day)
    end
    write_attribute(:dob, dob)
  end

  def coach_unreplied_messages_count
    last_coach_message = messages.last
    if last_coach_message.present?
      sent_messages.where("send_at > ?", last_coach_message.send_at).where(state: :confirmed).count
    else
      0
    end
  end

  def client_unreplied_messages_count
    last_client_message = sent_messages.last
    if last_client_message.present?
      messages.where("send_at > ?", last_client_message.send_at).where(state: :confirmed).count
    else
      0
    end
  end

  def related_messages
    Message.where("(sender_type = 'Client' AND sender_id = ?) OR (receiver_type = 'Client' AND receiver_id = ?)", id, id)
  end

  def past_messages
    self.related_messages.
      where("send_at <= ?", Time.zone.now).
      where(state: "confirmed").
      order("created_at DESC")
      
      #page(params[:page])
  end

  def messaged_last?
    if self.messages.any? && self.sent_messages.any?
      self.sent_messages.last.send_at > self.messages.last.send_at
    elsif self.sent_messages.any? && self.messages.empty?
      true
    else
      false
    end
  end

  def to_param
    "#{id}-#{first_name.downcase}-#{surname.downcase}"
  end
end
