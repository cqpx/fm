class ClientMailer < ActionMailer::Base
  default from: "no-reply@toptrainer.co.nz"

  def new_message msg
    @msg = msg

    if @msg.sender.is_a? Coach
      client_id = @msg.receiver.id
    elsif @msg.sender.is_a?(Client) || @msg.sender.is_a?(Group)
      client_id = @msg.sender.id
    end

    mail to: %Q{"#{msg.receiver.name}" <#{msg.receiver.email}>},
      from: %Q{"#{msg.sender.name}" <C#{client_id}-MB@toptrainer.co.nz>},
      subject: msg.title || "You've got a message!"
  end
end
