class InvitationMailer < ActionMailer::Base
  default from: "from@example.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invitation_mailer.invite_client.subject
  #
  def invite_client client_id
    @client = Client.find client_id

    mail to: @client.email, subject: "Welcome #{@client.first_name} to #{@client.coach.brand_name}"
  end
end
