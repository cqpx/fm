#= require jquery
#= require jquery_ujs
#= require jquery.mobile
#= require mobiscroll.custom-2.4.4.min.js
#= require raphael-min
#= require morris
#= require select2

msgBox = ->
  $("<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Sorry, what was incorrect. Please try again.</h1></div>").css(
    "display": "block"
    "opacity": 0.96
    "top": $(window).scrollTop() + 100
  ).appendTo( $.mobile.pageContainer ).delay( 1500 ).fadeOut( 400, ->
    $(this).remove();
  );

ready = ->
  $('.chart').each (index, ele) ->
    Morris.Line
      element: $(ele).attr('id')
      data: $(ele).data('checkins')
      xkey: 'checkin_at'
      ykeys: ['checkin_count']
      labels: ['Checkins']
      smooth: false
  $(".date_picker").mobiscroll().date(dateFormat: "yy-mm-dd")
  $(".time_picker").mobiscroll().time()

  onSuccess = (e, data) ->

  $("input[type=checkbox]").click (e) ->
    form = $(this).parents('form')
    $.ajax({
      type: "PUT",
      url: form.attr("action"),
      data: form.serialize(),
      cache: false,
      dataType: "text/json",
      success: onSuccess
    });

window.goBack = ->
  history.back()
  false

$(document).delegate '.ui-page', 'pageshow', ready
