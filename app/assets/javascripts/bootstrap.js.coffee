jQuery.fn.submitOnCheck = ->
  #@find('input[type=submit]').remove()
  @find('input[type=checkbox]').click ->
    $(this).parents('form').submit()
  this

ready = ->
  $("a[rel=popover]").popover()
  $(".tooltip").tooltip()
  $("a[rel=tooltip]").tooltip()
  $(".chzn-select").select2(width: "resolve")
  $('#checkin_box form').submitOnCheck()
  $("form").on 'click', '.add_fields', (event) ->
    time = new Date().getTime()
    regex = new RegExp($(this).data('id'), 'g')
    $(this).before($(this).data('fields').replace(regex, time))
    event.preventDefault()

  $("#expert-options-toggle").click (e)->
    e.preventDefault()
    $("#expert-options").toggleClass "hide"

  $(document).on("focus", "[data-behaviour~='datepicker-future']", (e) ->
    $(this).datepicker({
      "format": "yyyy-mm-dd",
      "weekStart": 1,
      "autoclose": true,
      "startDate":'+1D',
      "autoclose": true
    })
  )

  $(document).on("focus", "[data-behaviour~='datepicker-dob']", (e) ->
    $(this).datepicker({
      "format": "yyyy-mm-dd",
      "weekStart": 1,
      "viewMode":2,
      "autoclose": true
    })
  )

  $(document).on("focus", "[data-behaviour~='datepicker']", (e) ->
    $(this).datepicker({
      "format": "yyyy-mm-dd",
      "weekStart": 1,
      "autoclose": true
    })
  )

  $(document).on("focus", "[data-behaviour~='timepicker']", (e) ->
    $(this).timepicker()
  )

  unless $('#checkin_chart').length == 0
    Morris.Line
      element: 'checkin_chart'
      data: $('#checkin_chart').data('checkins')
      xkey: 'checkin_at'
      ykeys: ['checkin_count']
      labels: ['Checkins']
      smooth: false

  $(".fixed").stick() if $(".fixed").length > 0

  $(".modal").on('shown', ->
    $(".select2-select").select2(width: 'resolve')
  ).on('hidden', ->
    $(this).removeData('modal')
  )

$(document).on('page:load', ready)
$(document).ready(ready)
