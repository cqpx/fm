module ClientsHelper
  def format_message_detail message
    if message.title.present?
      title = "<strong>" + message.title + "</strong><br />"
    else
      title = ""
    end
    content = message.content.gsub(/\n/, '<br/>') if message.content.present?
    [title, content].join(' ').html_safe
  end

  def format_message_info message
    "Email from #{message.sender.name} at #{time_ago_in_words message.send_at} ago"
  end

  def format_checkin_time client
    time_ago_in_words(client.checkins.where(checked: true).last.updated_at) + ' ago' if client.checkins.where(checked: true).any?
  end

  def checkins_chart_data client
    (4.weeks.ago.to_date..Date.today).map do |date|
      {
        checkin_at: date,
        checkin_count: client.checkins.where(checked: true).where("date(checkins.updated_at) = ?", date).count
      }
    end
  end

  def link_to_add_fields name, f, association
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def fancy_birthday(bir)
    day = bir.change(year: Date.today.year)
    fancyday = day - Date.today

    if fancyday < 0
      day = bir.change(year: Date.today.year+1)
      fancyday = day - Date.today
    end
    "in #{fancyday.to_i} days time"
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current_#{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    chevron_direction = direction == "desc" ? "up" : "down"
    link_to %Q{#{title}<i class="icon-chevron-#{chevron_direction}"></i>}.html_safe, {:sort => column, :direction => direction}, :class => css_class
  end
end
