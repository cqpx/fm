module GoalsHelper

  def deadline_nice goal
    if !@active_goal.deadline.blank?
      content = "by " + goal.deadline.to_s(:rfc822) + " (about " + distance_of_time_in_words(goal.deadline, Time.now) + " left)"
    else
      content = ""
    end
  end

end