module GroupsHelper
  def format_message_detail message
    if message.title.present?
      title = "<strong>" + message.title + "</strong><hr />"
    else
      title = ""
    end
    content = message.content.gsub(/\n/, '<br/>') if message.content.present?
    [title, content].join(' ').html_safe
  end

  def format_message_info message
    "Email from #{message.sender.name} at #{time_ago_in_words message.created_at} ago"
  end
end
