class Coaches::RegistrationsController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)
    if mobile?
      scheduled_messages_path
    elsif resource.is_a?(Coach)
      coach_steps_path
    end
  end
end
