class GroupsController < ApplicationController
  before_filter :authenticate_coach!

  def index
    @active_groups = current_coach.groups.active
    @inactive_groups = current_coach.groups.inactive
  end

  def show
    @group = current_coach.groups.find(params[:id])
    @scheduled_messages = @group.
      messages.
      where("send_at > ?", Time.zone.now).
      order("send_at").
      page(params[:page])
    @sent_messages = @group.
      messages.
      where("send_at <= ?", Time.zone.now).
      order("send_at DESC").
      page(params[:page])
  end

  def info
    @group = current_coach.groups.find(params[:id])
  end

  def new
    @group = current_coach.groups.new
  end

  def edit
    @group = current_coach.groups.find(params[:id])
  end

  def create
    @group = current_coach.groups.new(params[:group])

    if @group.save
      redirect_to @group, notice: 'Group was successfully created.'
    else
      render "new"
    end
  end

  def update
    @group = current_coach.groups.find(params[:id])

    if @group.update_attributes(params[:group])
      redirect_to @group, notice: 'Group was successfully updated.'
    else
      render "edit"
    end
  end

  def destroy
    @group = current_coach.groups.find(params[:id])
    @group.destroy
    redirect_to coach_groups_path(current_coach)
  end

  def active
    @group = current_coach.groups.find(params[:id])
    @group.activate
    redirect_to :back, notice: 'Group was successfully activated.'
  end

  def deactive
    @group = current_coach.groups.find(params[:id])
    @group.deactivate
    redirect_to :back, notice: 'Group was successfully deactivated.'
  end
end
