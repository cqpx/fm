class GoalsController < ApplicationController
  before_filter :find_client_and_authenticate_ownership, except: [:new, :create]
  before_filter :find_client, only: [:new, :create]

  def new
    @goal = @client.goals.new
    render layout: false
  end

  def edit
    @goal = @client.goals.find(params[:id])
    render layout: false
  end

  # POST /goals
  # POST /goals.json
  def create
    @goal = @client.goals.new(params[:goal])
    if @client.goals.active.count == 0
      @goal.state = :active
    end

    respond_to do |format|
      if @goal.save
        format.html { redirect_to :back, notice: 'Goal was successfully created.' }
        format.json { render json: @goal, status: :created, location: @goal }
      else
        format.html { redirect_to :back, alert: "This goal was failed created." }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /goals/1
  # PUT /goals/1.json
  def update
    @goal = @client.goals.find(params[:id])

    respond_to do |format|
      if @goal.update_attributes(params[:goal])
        format.html { redirect_to :back, notice: 'Goal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1
  # DELETE /goals/1.json
  def destroy
    @goal.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  def active
    @goal = @client.goals.find params[:id]
    @goal.activate
    redirect_to :back, notice: 'This goal was successfully activated.'
  end

  def deactive
    @goal = @client.goals.find params[:id]
    @goal.deactivate
    redirect_to :back, notice: 'This goal was successfully deactivated.'
  end

private
  def find_client_and_authenticate_ownership
    if current_client
      @goal = current_client.goals.find(params[:id])
      @client = current_client
    elsif current_coach
      @goal = Goal.find params[:id]
      @client = current_coach.clients.find @goal.client.id
    else
      redirect_to new_user_session_path, notice: "LOGIN!!"
    end
  end

  def find_client
    if current_coach
      @client = current_coach.clients.find params[:client_id]
    else
      @client = current_client
    end
  end
end
