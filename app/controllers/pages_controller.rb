class PagesController < ApplicationController
  def index
    if coach_signed_in?
      redirect_to coach_clients_path(current_coach)
    elsif client_signed_in?
      redirect_to dashboard_client_path(current_client)
    end
  end

  def contact
  end
  
  def about
  end
  
  def faq
  end  
  
end
