class HabitsController < ApplicationController
  before_filter :find_client_and_authenticate_ownership, except: [:create, :new]
  before_filter :find_client, only: [:new, :create]

  def new
    render layout: false
  end

  def edit
    render layout: false
  end

  def create
    @habit = @client.habits.new(params[:habit])
    @habit.state = "inactive" if @client.habits.active.count >= 3
    if @habit.save
      redirect_to :back, notice: "This habit was successfully created."
    else
      redirect_to :back, notice: "This habit was failed to be created."
    end
  end

  def update
    @habit = @client.habits.find params[:id]
    respond_to do |format|
      if @habit.update_attributes(params[:habit])
        format.html { redirect_to :back, notice: 'This habit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @habit.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @habit = @client.habits.find params[:id]
    @habit.destroy
    redirect_to :back, notice: "Habit removed"
  end

  def active
    if @client.habits.active.count >= 3
      redirect_to :back, notice: 'There should be 3 active habits at most.'
    else
      @habit = @client.habits.find params[:id]
      @habit.activate
      redirect_to :back, notice: 'This habit was successfully activated.'
    end
  end

  def deactive
    @habit = @client.habits.find params[:id]
    @habit.deactivate
    redirect_to :back, notice: 'This habit was successfully deactivated.'
  end

  private
  def find_client_and_authenticate_ownership
    if current_client
      @habit = current_client.habits.find(params[:id])
      @client = current_client
    elsif current_coach
      @habit = Habit.find params[:id]
      @client = current_coach.clients.find @habit.client.id
    else
      redirect_to new_user_session_path, notice: "LOGIN!!"
    end
  end

  def find_client
    if current_coach
      @client = current_coach.clients.find params[:client_id]
    else
      @client = current_client
    end
  end
end
