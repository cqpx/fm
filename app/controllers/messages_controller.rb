class MessagesController < ApplicationController
  BODY_SPLIT = ":: Reply above this line ::"
  before_filter :load_messagable, only: [:index, :new, :scheduled]

  def index
    # if messagable is available, it's viewing that messagable's messages
    # if not, it's creating message for multiple clients and groups,
    # or sending message to his coach if client logged in instead of coach
    redirect_to root_path unless coach_signed_in? || client_signed_in?

    # past_messages = all messages in the past AND confirmed
    # scheduled_messages = everything else (NOT (past AND confirmed))

    if mobile?
      if coach_signed_in?
        render "messages/index"
      else
        render "clients/messages"
      end
    end
  end

  def reply
    @message = Message.find params[:id]
  end

  def scheduled
    
  end

  def new
    if mobile?
      if @messagable.present?
        logger.debug("A")
        logger.debug(@messagable.inspect)
        render
      else
        logger.debug("B")
        render "new_batch_messages"
      end
    else
      if @messagable.present?
        logger.debug("A")
        logger.debug(@messagable.inspect)
        render layout: false
      else
        logger.debug("B")
        render "new_batch_messages", layout: false
      end
    end
  end

  def edit
    @message = Message.find params[:id]
    if mobile?
      render
    else
      render layout: false
    end
  end

  def create
    if coach_signed_in?
      if params[:client_ids] || params[:group_ids]
        Message.send_to_clients params[:message], params[:client_ids], current_coach
        Message.send_to_groups params[:message], params[:group_ids], current_coach
        redirect_to :back, notice: "Top Trainer successfully created your message."
      else
        receiver = current_coach.clients.where(id: params[:client_id]).first || current_coach.groups.where(id: params[:group_id]).first
        unless receiver.present?
          redirect_to :back, alert: "Top Trainer failed to create your message."
          return
        end
        message = receiver.messages.new params[:message]
        message.sender = current_coach

        if message.save
          redirect_to :back, notice: "Top Trainer successfully created your message."
        else
          redirect_to :back, alert: "Top Trainer failed to create your message."
        end
      end
    elsif client_signed_in?
      coach = Coach.find_by_id current_client.coach.id
      message = coach.messages.new params[:message]
      message.state = :confirmed
      message.sender = current_client
      message.message_type = 1 if mobile?
      if message.save
        redirect_to :back, notice: "Top Trainer successfully created your message."
      else
        redirect_to :back, alert: "Top Trainer failed to create your message."
      end
    else
      redirect_to root_path, alert: "Login please"
    end
  end

  def update
    @message = Message.find(params[:id])

    if (@message.sender == current_coach || @message.sender == current_client) && @message.update_attributes(params[:message])
      redirect_to :back, notice: 'This message was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @message = Message.find(params[:id])
    receiver = @message.receiver

    if coach_signed_in? && ( @message.sender == current_coach || @message.receiver == current_coach )
      @message.destroy
    elsif client_signed_in? && ( @message.sender == current_client || @message.receiver == current_client)
      @message.destroy
    end

    respond_to do |format|
      format.html { redirect_to receiver }
      format.js
    end
  end

  def inbound
    event = JSON.parse(params[:mandrill_events])[0]
    recipient_email = event['msg']['to'][0][0]
    sender_email = event['msg']['from_email']

    recipient_email.match(/C(\d+)-MB@/)
    email_body = MessagesController.clean_body(event['msg']['text'])
    if $1.present?
      client = Client.find $1

      coach = Coach.find_by_email(sender_email)
      if coach.present? && coach.clients.includes?(client)
        client.messages.create(content: email_body, sender: coach)
      elsif client.email == sender_email
        client.coach.messages.create(content: email_body, sender: client)
      end
    end

    render nothing: true
  end

  def confirm
    @message = Message.find(params[:id])
    if @message.confirmed?
      @message.unconfirm
    else
      @message.confirm
    end
    respond_to do |format|
      if mobile?
        format.html { redirect_to scheduled_coach_messages_path(current_coach),
          notice: 'This piece of message was successfully confirmed.'}
        format.js
      else
        format.html { redirect_to :back,
          notice: 'This piece of message was successfully confirmed.'}
        format.js
      end
    end
  end

  private

  def self.clean_body(body)
    new_body_end = body.to_s.index(BODY_SPLIT) || body.to_s.length
    body = body.to_s[0, new_body_end].strip

    lines = body.to_s.split("\n")
    while lines.any?
      line = lines.last.strip

      if line.blank? or line.match(/^[<>]+$/) or line.match(/.* wrote:/)
        lines.pop
      else
        break
      end
    end

    return lines.join("\n")
  end

  # alternative option:
  def load_messagable
    klass = [Client, Coach, Group].detect { |c| params["#{c.name.underscore}_id"] }
    if klass.present?
      @messagable = klass.find(params["#{klass.name.underscore}_id"])
    end
  end
end
