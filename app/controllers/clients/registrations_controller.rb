class Clients::RegistrationsController < Devise::RegistrationsController
  def create
    build_resource
    client = Client.find_by_email resource.email

    if client.present? && client.encrypted_password.blank?
      client.update_attributes(
        password: resource.password,
        password_confirmation: resource.password_confirmation,
        first_name: resource.first_name,
        surname: resource.surname,
        phone_number: resource.phone_number,
      )
      set_flash_message :notice, :signed_up if is_navigational_format?
      sign_in(resource_name, client)
      respond_with client, :location => after_sign_in_path_for(client)
    elsif resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end
end
