class ClientsController < ApplicationController
  before_filter :authenticate_coach!, only: [:show]
  before_filter do
    if current_coach != nil
     authenticate_coach!
    else
     authenticate_client!
    end
  end

  before_filter :find_client, only: [:edit, :info, :goals, :show, :update]
  helper_method :sort_column, :sort_direction

  def index
    @active_clients = current_coach.clients.active.
      order(sort_column + " " + sort_direction)
    @inactive_clients = current_coach.clients.inactive.
      order(sort_column + " " + sort_direction)
  end

  def dashboard
    @client = current_client
    @messages = @client.related_messages.
      where("send_at <= ?", Time.zone.now).
      where(state: "confirmed").
      order("created_at DESC").
      page(params[:page])
    @unreplied_messages_count = @client.client_unreplied_messages_count
    @today_checkins = @client.today_checkins
    @latest_checkins = @client.latest_checkins
    @active_goal = @client.goals.active.first
  end

  def checkins
    @checkins = current_coach.checkins.limit(50)
  end

  def info
  end

  def goals
    @habits = @client.habits.order("id DESC")
    @inactive_habits = @client.habits.inactive
    @active_habits = @client.habits.active
    @inactive_goals = @client.goals.inactive
    @active_goal = @client.goals.active.last
    @today_checkins = @client.today_checkins
    @latest_checkins = @client.checkins.where(checked: true).order("updated_at DESC").limit(3)
  end

  def show
    @messages = @client.related_messages.
      where("send_at <= ?", Time.zone.now).
      where(state: "confirmed").
      order("created_at DESC").
      page(params[:page])
    @scheduled_messages = @client.
      messages.
      where("send_at > ?", Time.zone.now).
      order("created_at DESC").
      page(params[:page])
    @today_checkins = @client.today_checkins
    @latest_checkins = @client.checkins.where(checked: true).order("updated_at DESC").limit(3)
    @unreplied_messages_count = @client.coach_unreplied_messages_count
    @active_goal = @client.goals.active.first
  end

  def new
    @client = current_coach.clients.new
  end

  def edit
  end

  def create
    @client = current_coach.clients.new(params[:client])

    if @client.save
      if mobile?
        redirect_to :back, notice: 'Client was successfully created.'
      else
        redirect_to @client, notice: 'Client was successfully created.'
      end
    else
      render "new"
    end
  end

  def update
    if @client.update_attributes(params[:client])
      if coach_signed_in?
        if params[:client][:checkins_attributes].present?
          redirect_to goals_client_path(@client)
        else
          redirect_to info_client_path(@client)
        end
      else
        respond_to do |f|
          f.html { redirect_to dashboard_client_path(@client) }
          f.json {
            render :json => {
              :status => :ok,
              :message => "Success!",
              :html => ""
            }.to_json
          }
        end
      end
    else
      render "edit"
    end
  end

  def destroy
    @client = current_coach.clients.find(params[:id])
    @client.destroy

    redirect_to coach_clients_path(current_coach)
  end

  def active
    @client = current_coach.clients.find(params[:id])
    @client.activate
    redirect_to :back, notice: 'Client was successfully activated.'
  end

  def deactive
    @client = current_coach.clients.find(params[:id])
    @client.deactivate
    redirect_to :back, notice: 'Client was successfully deactivated.'
  end

  private

  def find_client
    if coach_signed_in?
      @client = current_coach.clients.find(params[:id])
    elsif client_signed_in?
      @client = current_client
    else
      redirect_to root_path
    end
  end

  def sort_column
    params[:sort] || "first_name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

end
