class CoachStepsController < ApplicationController
  include Wicked::Wizard
  steps :clients, :groups, :messages
  before_filter :authenticate_coach!

  def show
    @coach = current_coach
    case step
    when :clients
      5.times { @coach.clients.build }
    end
    render_wizard
  end

  def update
    @coach = current_coach
    case step
    when :clients
      @coach.attributes = params[:coach]
    when :groups
      params[:coach][:groups_attributes].each do |k, v|
        if v[:name].blank?
          params[:coach][:groups_attributes].delete k
        end
      end
      @coach.attributes = params[:coach]
    when :messages
      Message.send_to_clients params[:message], params[:client_ids], current_coach
      Message.send_to_groups params[:message], params[:group_ids], current_coach
    end

    render_wizard @coach
  end
end
