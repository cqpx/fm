class CoachesController < ApplicationController
  def edit_profile
    @coach = current_coach
  end

  def update_profile
    @coach = Coach.find(current_coach.id)
    if @coach.update_attributes(params[:coach])
      # Sign in the coach by passing validation in case his password changed
      sign_in @coach, :bypass => true
      redirect_to settings_coaches_path
    else
      render "edit_profile"
    end
  end

  def settings
  end

  def dashboard
  end
  
end
